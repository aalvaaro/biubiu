require 'faker'

User.destroy_all
Office.delete_all
Hairstyle.delete_all
Makeup.delete_all
Nail.delete_all


puts 'Creating admin...'
User.create!(
    email: "admin@email.com",
    password: 'qwertyuiop',
    name: 'Elia',
    lastname: 'Gonzalez',
    is_admin: true
)

