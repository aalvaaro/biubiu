class AddFullAddressToOffices < ActiveRecord::Migration
  def change
    add_column :offices, :full_address, :string
  end
end
