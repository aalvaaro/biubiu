class CreateNails < ActiveRecord::Migration
  def change
    create_table :nails do |t|
      t.string :name
      t.string :duration
      t.decimal :cost
      t.text :detail

      t.timestamps null: false
    end
  end
end
