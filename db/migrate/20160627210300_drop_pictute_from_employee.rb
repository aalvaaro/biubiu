class DropPictuteFromEmployee < ActiveRecord::Migration
  def change
    remove_column :employees, :picture
  end
end
