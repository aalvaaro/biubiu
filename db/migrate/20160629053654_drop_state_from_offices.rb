class DropStateFromOffices < ActiveRecord::Migration
  def change
    remove_column :offices, :state
  end
end
