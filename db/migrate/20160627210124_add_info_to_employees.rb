class AddInfoToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :nie, :string
    add_column :employees, :latitude, :float
    add_column :employees, :longitude, :float
    add_column :employees, :social_security_number, :string
    add_column :employees, :bank, :string
    add_column :employees, :account_number, :string
  end
end
