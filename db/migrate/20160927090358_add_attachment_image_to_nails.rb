class AddAttachmentImageToNails < ActiveRecord::Migration
  def self.up
    change_table :nails do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :nails, :image
  end
end
