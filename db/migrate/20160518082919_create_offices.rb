class CreateOffices < ActiveRecord::Migration
  def change
    create_table :offices do |t|
      t.string :name
      t.string :city
      t.string :address
      t.string :zip_code
      t.float :latitude
      t.float :longitude

      t.timestamps null: false
    end
  end
end
