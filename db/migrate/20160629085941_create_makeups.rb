class CreateMakeups < ActiveRecord::Migration
  def change
    create_table :makeups do |t|
      t.string :name
      t.string :duration
      t.decimal :cost, :precision => 8, :scale => 2

      t.timestamps null: false
    end
  end
end
