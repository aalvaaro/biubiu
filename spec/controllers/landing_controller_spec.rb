require 'rails_helper'

RSpec.describe LandingController, type: :controller do

  describe "GET #home" do
    it "returns http success" do
      get :home
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #hairstyles" do
    it "returns http success" do
      get :hairstyles
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #makeups" do
    it "returns http success" do
      get :makeups
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #checkout" do
    it "returns http success" do
      get :checkout
      expect(response).to have_http_status(:success)
    end
  end

end
