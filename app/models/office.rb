class Office < ActiveRecord::Base
  has_many :employees

  validates_presence_of :name, :city, :address, :zip_code, :phone

  geocoded_by :full_address
  after_validation :generate_full_address
  before_save :geocode


  private

  def generate_full_address
    self.full_address = "#{self.city} #{self.address} #{self.zip_code}"
  end

end
