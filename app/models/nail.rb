class Nail < ActiveRecord::Base
  has_attached_file :image, styles: { medium: "300x300#", thumb: "100x100#" }, default_url: "nails.jpg",
                    :url => ':s3_domain_url', :path => '/:class/:attachment/:id_partition/:style/:filename'
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  validates_presence_of :name, :duration, :cost, :detail
end
