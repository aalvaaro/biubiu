class Employee < ActiveRecord::Base
  validates_presence_of :name, :phone, :office_id, :social_security_number, :bank, :account_number
  validates :email, presence: true, format: { with: /\A[^@]+@[^@]+\z/}

  belongs_to :office
  has_attached_file :avatar, styles: { medium: "200x200#", thumb: "100x100>" }, default_url: "employee.jpg",
                    :url => ':s3_domain_url', :path => '/:class/:attachment/:id_partition/:style/:filename'
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  validates_presence_of :name, :office_id, :nie, :social_security_number, :bank, :account_number,
                        :email, :phone, :address, :city, :zip

  geocoded_by :full_address
  after_validation :generate_full_address
  before_save :geocode


  private

  def generate_full_address
    self.full_address = "#{self.city} #{self.address} #{self.zip}"
  end
end
