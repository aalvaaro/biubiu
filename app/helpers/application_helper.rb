module ApplicationHelper
  def custom_flash_messages
    flash_messages= []
    flash.each do |type, msg|
      type = "success" if type == "notice"
      type = "error" if type == "alert"

      text = "<script>toastr.#{type}(#{msg});</script>"
      flash_messages << text.html_safe if msg
    end

    flash_messages.join("\n").html_safe
  end

  def offices_list
    Office.all.collect { |office| [office[:name], office[:id]] }
  end
end 
