class Admin::MakeupsController < ApplicationController
  include AdminValidation
  before_action :authenticate_user!, :is_admin?
  before_action :get_makeup, except: [:index, :create, :new]

  def index
    @makeups = Makeup.all
  end

  def new
    @makeup = Makeup.new
  end

  def create
    @makeup = Makeup.new(makeup_params)

    if @makeup.save
      redirect_to [:admin, @makeup]
    else
      render new_admin_makeup_path
    end
  end

  def show
  end

  def edit
  end

  def update
    @makeup.update(makeup_params)
    if @makeup.persisted?
      flash[:notice] = "Se ha actualizado exitosamente"
      redirect_to admin_makeup_path
    end
  end


  def destroy
    @makeup.destroy!
    redirect_to admin_makeups_path
  end

  private

  def get_makeup
    @makeup = Makeup.find(params[:id])
  end

  def makeup_params
    params.require(:makeup).permit(:name, :duration, :cost, :avatar)
  end
end
