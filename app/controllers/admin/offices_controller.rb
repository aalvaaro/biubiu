class Admin::OfficesController < ApplicationController
  include AdminValidation
  before_action :authenticate_user!, :is_admin?
  before_action :get_office, except: [:index, :create, :new]

  def index
    @offices = Office.all
  end

  def new
    @office = Office.new
  end

  def create
    @office = Office.new(office_params)

    if @office.save
      redirect_to [:admin, @office]
    else
      render new_admin_office_path
    end
  end

  def show
  end

  def edit
  end

  def update
    @office.update(office_params)
    if @office.persisted?
      flash[:notice] = "Se ha actualizado exitosamente"
      redirect_to admin_office_path
    end
  end


def destroy
    @office.destroy!
    redirect_to admin_offices_path
  end

  private

  def get_office
    @office = Office.find(params[:id])
  end

  def office_params
    params.require(:office).permit(:name, :city, :address, :zip_code, :phone)
  end
end
