class Admin::NailsController < ApplicationController
  include AdminValidation
  before_action :authenticate_user!, :is_admin?
  before_action :get_nail, except: [:index, :create, :new]

  def index
    @nails = Nail.all
  end

  def new
    @nail = Nail.new
  end

  def create
    @nail = Nail.new(nail_params)

    if @nail.save
      redirect_to [:admin, @nail]
    else
      render new_admin_nail_path
    end
  end

  def show
  end

  def edit
  end

  def update
    @nail.update(nail_params)
    if @nail.persisted?
      flash[:notice] = "Se ha actualizado exitosamente"
      redirect_to admin_nails_path
    end
  end


  def destroy
    @nail.destroy!
    redirect_to admin_nails_path
  end

  private

  def get_nail
    @nail = Nail.find(params[:id])
  end

  def nail_params
    params.require(:nail).permit(:name, :duration, :cost, :detail, :image)
  end
end
