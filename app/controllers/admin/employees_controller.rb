class Admin::EmployeesController < ApplicationController
  include AdminValidation
  before_action :authenticate_user!, :is_admin?
  before_action :get_employee, except: [:index, :create, :new]

  def index
    @employees = Employee.paginate(page: params[:page], per_page: 10)
  end

  def new
    @employee = Employee.new
  end

  def create
    @employee = Employee.new(employee_params)

    if @employee.save
      redirect_to [:admin, @employee]
    else
      render new_admin_employee_path
    end
  end

  def show
  end

  def edit
  end

  def update
    if @employee.update(employee_params)
      redirect_to admin_employee_path
    else
      render 'edit'
    end
  end

  def destroy
    @employee.destroy
    redirect_to root_path, notice: 'Estilista ha sido eliminado'
  end

  private

  def get_employee
    @employee = Employee.find(params[:id])
  end

  def employee_params
    params.require(:employee).permit(:name, :phone, :picture, :office_id, :id, :avatar, :city, :address, :zip, :email,
                                     :nie, :social_security_number, :bank, :account_number)
  end
end
