class Admin::HairstylesController < ApplicationController
  include AdminValidation
  before_action :authenticate_user!, :is_admin?
  before_action :get_hairstyle, except: [:index, :create, :new]

  def index
    @hairstyles = Hairstyle.all
  end

  def new
    @hairstyle = Hairstyle.new
  end

  def create
    @hairstyle = Hairstyle.new(hairstyle_params)

    if @hairstyle.save
      redirect_to [:admin, @hairstyle]
    else
      render new_admin_hairstyle_path
    end
  end

  def show
  end

  def edit
  end

  def update
    @hairstyle.update(hairstyle_params)
    if @hairstyle.persisted?
      flash[:notice] = "Se ha actualizado exitosamente"
      redirect_to admin_hairstyle_path
    end
  end


  def destroy
    @hairstyle.destroy!
    redirect_to admin_hairstyles_path
  end

  private

  def get_hairstyle
    @hairstyle = Hairstyle.find(params[:id])
  end

  def hairstyle_params
    params.require(:hairstyle).permit(:name, :duration, :cost, :detail, :image)
  end
end
