module AdminValidation
  def is_admin?
    redirect_to no_admin_show_path unless current_user.is_admin
  end
end