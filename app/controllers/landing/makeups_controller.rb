class Landing::MakeupsController < ApplicationController
  layout 'salon'

  def index
    @makeups = Makeup.all
  end
end