class Landing::OfficesController < ApplicationController
  layout 'salon'

  def index
    @offices = Office.all
  end

  def checout
    if params[:harstyle_id]
      @cart = Hairstyle.find(params[:harstyle_id])
    else
      @cart = Makeup.find(params[:makeup_id])
    end
  end
end