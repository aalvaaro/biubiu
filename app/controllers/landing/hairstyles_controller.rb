class Landing::HairstylesController < ApplicationController
  layout 'salon'

  def index
    @hairstyles = Hairstyle.all
  end
end