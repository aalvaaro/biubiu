class Landing::NailsController < ApplicationController
  layout 'salon'

  def index
    @nails = Makeup.all
  end
end