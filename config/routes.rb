Rails.application.routes.draw do

  get 'no_admin/show'

  devise_for :users, :controllers => {
      :omniauth_callbacks => "omniauth_callbacks",
      :registrations => 'users/registrations',
      :sessions => 'users/sessions'
  }

  root 'landing#home'

  namespace :landing do
    resources :hairstyles, only: [:index, :show] do
      resources :offices, only: [:index] do
        member do
          get :checkout
        end
      end
    end

    resources :makeups, only: [:index, :show] do
      resources :offices, only: [:index] do
        get :checkout
      end
    end

    resources :nails, only: [:index, :show] do
      resources :offices, only: [:index] do
        get :checkout
      end
    end

  end


  namespace :admin do
    resources :offices
    resources :employees
    resources :hairstyles
    resources :makeups
    resources :nails
  end

end


